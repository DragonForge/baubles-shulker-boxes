package com.zeitheron.baublesb.boxes;

import com.zeitheron.baublesb.cap.BaubleImpl;

import net.minecraft.block.Block;
import net.minecraft.block.BlockShulkerBox;
import net.minecraft.client.renderer.entity.RenderShulker;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class VanillaShulkerBox extends BaubleImpl
{
	public VanillaShulkerBox(ItemStack stack)
	{
		super(stack, null, 27);
	}

	@Override
	public int getSlotCount(ItemStack stack)
	{
		return 27;
	}
	
	@Override
	public ResourceLocation getBoxTextures(ItemStack stack)
	{
		return RenderShulker.SHULKER_ENDERGOLEM_TEXTURE[BlockShulkerBox.getColorFromBlock(Block.getBlockFromItem(stack.getItem())).getMetadata()];
	}
}