package com.zeitheron.baublesb.boxes;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Random;

import com.zeitheron.baublesb.BSBUtil;
import com.zeitheron.baublesb.BaubleShulkerBoxes;
import com.zeitheron.baublesb.cap.BaubleImpl;
import com.zeitheron.baublesb.cap.IShulkerAnimation;
import com.zeitheron.baublesb.intr.ironchest.GuiIronShulkerBox;

import cpw.mods.ironchest.common.blocks.shulker.IronShulkerBoxType;
import cpw.mods.ironchest.common.gui.shulker.ContainerIronShulkerBox;
import cpw.mods.ironchest.common.items.shulker.ItemIronShulkerBox;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class IronShulkerBox extends BaubleImpl
{
	public final String color;
	
	public IronShulkerBox(ItemStack stack)
	{
		super(stack, null, 0);
		
		String colorName = "";
		
		try
		{
			Field f = ItemIronShulkerBox.class.getDeclaredField("colorName");
			f.setAccessible(true);
			colorName = "" + f.get(stack.getItem());
		} catch(Throwable err)
		{
		}
		
		color = colorName;
	}
	
	@Override
	public int getSlotCount(ItemStack stack)
	{
		return getType(stack).size;
	}
	
	@Override
	public Object createGui(EntityPlayer player, IInventory inv, ItemStack stack)
	{
		IronShulkerBoxType type = getType(stack);
		if(type == null)
			return null;
		return new GuiIronShulkerBox(player.inventory, inv, type, type.xSize, type.ySize);
	}
	
	@Override
	public Object createContainer(EntityPlayer player, IInventory inv, ItemStack stack)
	{
		IronShulkerBoxType type = getType(stack);
		if(type == null)
			return null;
		return new ContainerIronShulkerBox(player.inventory, inv, type, type.xSize, type.ySize);
	}
	
	public IronShulkerBoxType getType(ItemStack stack)
	{
		int meta = stack.getMetadata();
		return meta < IronShulkerBoxType.VALUES.length ? IronShulkerBoxType.VALUES[meta] : null;
	}
	
	@Override
	public ResourceLocation getBoxTextures(ItemStack stack)
	{
		IronShulkerBoxType type = getType(stack);
		if(type == null)
			return null;
		return new ResourceLocation("ironchest", "textures/model/shulker/" + color + "/shulker_" + color + type.modelTexture);
	}
	
	private Object itemRenderer;
	private static float[][] shifts = new float[][] { { 0.3F, 0.45F, 0.3F }, { 0.7F, 0.45F, 0.3F }, { 0.3F, 0.45F, 0.7F }, { 0.7F, 0.45F, 0.7F }, { 0.3F, 0.1F, 0.3F }, { 0.7F, 0.1F, 0.3F }, { 0.3F, 0.1F, 0.7F }, { 0.7F, 0.1F, 0.7F }, { 0.5F, 0.32F, 0.5F } };
	private static EntityItem customitem = new EntityItem(null);
	
	final Random random = new Random();
	
	@Override
	@SideOnly(Side.CLIENT)
	protected void renderWithOffset(ItemStack stack, EntityPlayer player, RenderType type, float partialTicks)
	{
		super.renderWithOffset(stack, player, type, partialTicks);
		
		IShulkerAnimation isa = player.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null);
		if(isa == null)
			return;
		NonNullList<ItemStack> topStacks = isa.getTopItems();
		if(topStacks == null)
			return;
		
		RenderEntityItem itemRenderer = BSBUtil.cast(this.itemRenderer, RenderEntityItem.class);
		if(itemRenderer == null)
			this.itemRenderer = itemRenderer = new RenderEntityItem(Minecraft.getMinecraft().getRenderManager(), Minecraft.getMinecraft().getRenderItem());
		
		IronShulkerBoxType sbt = getType(stack);
		
		if(sbt.isTransparent())
		{
			GlStateManager.enableCull();
			
			this.random.setSeed(254L);
			int shift = 0;
			float blockScale = 0.7F;
			float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
			if(topStacks.get(1).isEmpty())
			{
				shift = 8;
				blockScale = 0.85F;
			}
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(-.5F, .5F, -.5F);
			customitem.setWorld(player.world);
			customitem.hoverStart = 0;
			Iterator<ItemStack> var19 = topStacks.iterator();
			
			while(var19.hasNext())
			{
				ItemStack item = var19.next();
				if(shift > shifts.length || shift > 8)
					break;
				
				if(item.isEmpty())
					++shift;
				else
				{
					float shiftX = shifts[shift][0];
					float shiftY = shifts[shift][1];
					float shiftZ = shifts[shift][2];
					++shift;
					GlStateManager.pushMatrix();
					GlStateManager.translate(shiftX, shiftY, shiftZ);
					GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
					GlStateManager.scale(blockScale, blockScale, blockScale);
					customitem.setItem(item);
					
					itemRenderer.doRender(customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
					GlStateManager.popMatrix();
				}
			}
			
			GlStateManager.popMatrix();
		}
	}
}