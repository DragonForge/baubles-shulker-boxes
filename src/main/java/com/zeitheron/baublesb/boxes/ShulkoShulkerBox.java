package com.zeitheron.baublesb.boxes;

import com.unascribed.shulko.TileEntityShulkoShulkerBoxRenderer;
import com.zeitheron.baublesb.cap.BaubleImpl;

import net.minecraft.block.Block;
import net.minecraft.block.BlockShulkerBox;
import net.minecraft.client.renderer.entity.RenderShulker;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ShulkoShulkerBox extends BaubleImpl
{
	public ShulkoShulkerBox(ItemStack stack)
	{
		super(stack, new ResourceLocation("shulko", "textures/entity/shulker_actually_purple.png"), 27);
	}
}