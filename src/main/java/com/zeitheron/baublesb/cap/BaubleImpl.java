package com.zeitheron.baublesb.cap;

import com.zeitheron.baublesb.BaubleShulkerBoxes;
import com.zeitheron.baublesb.api.IContainer;
import com.zeitheron.baublesb.api.ISoundOpenable;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.IBauble;
import baubles.api.render.IRenderBauble;
import baubles.api.render.IRenderBauble.RenderType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiShulkerBox;
import net.minecraft.client.model.ModelShulker;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ContainerShulkerBox;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BaubleImpl implements IBauble, IRenderBauble, ISoundOpenable, IContainer
{
	public final ItemStack stack;
	
	public ResourceLocation texture;
	public int size;
	
	public BaubleImpl(ItemStack stackIn, ResourceLocation textureIn, int slotCount)
	{
		this.stack = stackIn;
		this.texture = textureIn;
		this.size = slotCount;
	}
	
	public ResourceLocation getBoxTextures(ItemStack stack)
	{
		return texture;
	}
	
	public int getSlotCount(ItemStack stack)
	{
		return size;
	}
	
	public InventoryItemShulkerBox getInventory(EntityPlayer player, ItemStack stack)
	{
		ItemStack item = BaublesApi.getBaublesHandler(player).getStackInSlot(BaubleType.BODY.getValidSlots()[0]);
		return !item.isEmpty() ? new InventoryItemShulkerBox(item, this)
		{
			@Override
			public int getSizeInventory()
			{
				return getSlotCount(stack);
			}
		} : null;
	}
	
	public Object createGui(EntityPlayer player, IInventory inv, ItemStack stack)
	{
		return new GuiShulkerBox(player.inventory, inv);
	}
	
	public Object createContainer(EntityPlayer player, IInventory inv, ItemStack stack)
	{
		return new ContainerShulkerBox(player.inventory, inv, player);
	}
	
	public SoundEvent getOpenSound()
	{
		return SoundEvents.BLOCK_SHULKER_BOX_OPEN;
	}
	
	public SoundEvent getCloseSound()
	{
		return SoundEvents.BLOCK_SHULKER_BOX_CLOSE;
	}
	
	protected boolean hasChestplate(EntityPlayer player)
	{
		return !player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).isEmpty();
	}
	
	Object model;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void onPlayerBaubleRender(ItemStack stack, EntityPlayer player, RenderType type, float partialTicks)
	{
		if(type != RenderType.BODY || stack.isEmpty())
			return;
		
		ResourceLocation tex = getBoxTextures(stack);
		
		if(tex == null)
			return;
		
		Minecraft mc = FMLClientHandler.instance().getClient();
		
		IRenderBauble.Helper.rotateIfSneaking(player);
		
		GlStateManager.scale(0.35D, 0.35D, 0.35D);
		GlStateManager.rotate(180.0F, 0.0F, 1.0F, 1.0F);
		GlStateManager.translate(0.0D, 1.84D + (hasChestplate(player) ? .2D : 0D), .9);
		
		// mc.getRenderItem().renderItem(stack, TransformType.NONE);
		
		{
			
			EnumFacing enumfacing = EnumFacing.UP;
			
			GlStateManager.enableDepth();
			GlStateManager.depthFunc(515);
			GlStateManager.depthMask(true);
			GlStateManager.disableCull();
			
			GlStateManager.pushMatrix();
			GlStateManager.enableRescaleNormal();
			
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1F);
			
			// GlStateManager.translate((float) x + 0.5F, (float) y + 1.5F,
			// (float) z + 0.5F);
			GlStateManager.scale(1.0F, -1.0F, -1.0F);
			GlStateManager.translate(0.0F, 1.0F, 0.0F);
			
			float f = 0.9995F;
			GlStateManager.scale(0.9995F, 0.9995F, 0.9995F);
			GlStateManager.translate(0.0F, -1.0F, 0.0F);
			
			switch(enumfacing)
			{
			case DOWN:
				GlStateManager.translate(0.0F, 2.0F, 0.0F);
				GlStateManager.rotate(180.0F, 1.0F, 0.0F, 0.0F);
			case UP:
			default:
			break;
			case NORTH:
				GlStateManager.translate(0.0F, 1.0F, 1.0F);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
			break;
			case SOUTH:
				GlStateManager.translate(0.0F, 1.0F, -1.0F);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
			break;
			case WEST:
				GlStateManager.translate(-1.0F, 1.0F, 0.0F);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(-90.0F, 0.0F, 0.0F, 1.0F);
			break;
			case EAST:
				GlStateManager.translate(1.0F, 1.0F, 0.0F);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(90.0F, 0.0F, 0.0F, 1.0F);
			}
			
			float progress = .5F;
			IShulkerAnimation isa = player.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null);
			if(isa != null)
				progress = isa.getCurrentProgress(partialTicks);
			
			ModelShulker model = this.model instanceof ModelShulker ? (ModelShulker) this.model : (ModelShulker) (this.model = new ModelShulker());
			
			mc.getTextureManager().bindTexture(tex);
			GlStateManager.pushMatrix();
			model.base.render(0.0625F);
			GlStateManager.translate(0.0F, -progress * 0.5F, 0.0F);
			GlStateManager.rotate(270.0F * progress, 0.0F, 1.0F, 0.0F);
			model.lid.render(0.0625F);
			GlStateManager.popMatrix();
			
			GlStateManager.pushMatrix();
			renderWithOffset(stack, player, type, partialTicks);
			GlStateManager.popMatrix();
			
			GlStateManager.enableCull();
			GlStateManager.disableRescaleNormal();
			GlStateManager.popMatrix();
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
	
	@SideOnly(Side.CLIENT)
	protected void renderWithOffset(ItemStack stack, EntityPlayer player, RenderType type, float partialTicks)
	{
		
	}
	
	@Override
	public BaubleType getBaubleType(ItemStack itemstack)
	{
		return BaubleType.BODY;
	}
}