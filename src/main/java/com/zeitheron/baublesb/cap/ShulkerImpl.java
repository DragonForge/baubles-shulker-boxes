package com.zeitheron.baublesb.cap;

import java.lang.ref.WeakReference;

import com.zeitheron.baublesb.NetworkBSB;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;

public class ShulkerImpl implements IShulkerAnimation
{
	public static float actionStep = .1F;
	
	public float progress;
	public boolean action;
	
	public WeakReference<EntityPlayer> playerRef;
	
	public NonNullList<ItemStack> topItems = NonNullList.withSize(8, ItemStack.EMPTY);
	
	@Override
	public void update()
	{
		EntityPlayer player = getPlayer();
		if(player instanceof EntityPlayerMP && !player.world.isRemote)
		{
			boolean open = false;
			
			if(player.openContainer != null)
			{
				InventoryItemShulkerBox inv = null;
				
				int k = 0;
				for(Slot s : player.openContainer.inventorySlots)
				{
					if(s.inventory instanceof InventoryItemShulkerBox)
					{
						inv = (InventoryItemShulkerBox) s.inventory;
						break;
					}
					
					// Still haven't found the correct inventory? Break!
					if(++k > 37)
						break;
				}
				
				open = inv != null;
				
				if(open)
				{
					topItems.clear();
					int j = 0;
					for(int i = 0; i < inv.getSizeInventory(); ++i)
					{
						ItemStack s = inv.getStackInSlot(i);
						if(!s.isEmpty())
						{
							topItems.set(j, s);
							++j;
							if(j >= topItems.size())
								break;
						}
					}
					if(!inv.cleaned)
					{
						inv.cleaned = true;
						NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
					}
				}
			}
			
			if(open != action)
			{
				action = open;
				NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
			}
		}
		
		if(action && progress < 1F)
			progress = Math.min(1F, progress + actionStep);
		else if(!action && progress > 0F)
			progress = Math.max(0F, progress - actionStep);
	}
	
	@Override
	public void setPlayer(EntityPlayer player)
	{
		playerRef = new WeakReference<>(player);
	}
	
	@Override
	public EntityPlayer getPlayer()
	{
		return playerRef != null ? playerRef.get() : null;
	}
	
	@Override
	public void open()
	{
		action = true;
		EntityPlayer player = getPlayer();
		if(player instanceof EntityPlayerMP)
			NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
	}
	
	@Override
	public void close()
	{
		action = false;
		EntityPlayer player = getPlayer();
		if(player instanceof EntityPlayerMP)
			NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
	}
	
	@Override
	public boolean getLastActionState()
	{
		return action;
	}
	
	@Override
	public float getProgress()
	{
		return progress;
	}
	
	@Override
	public float getCurrentProgress(float partialTime)
	{
		float f = Math.max(0, Math.min(1, getProgress() + (action ? 1 : -1) * partialTime * actionStep));
		return (float) Math.sin(Math.toRadians(f * 90));
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound n = new NBTTagCompound();
		n.setFloat("Progress", progress);
		n.setBoolean("Action", action);
		ItemStackHelper.saveAllItems(n, topItems, false);
		return n;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		progress = nbt.getFloat("Progress");
		action = nbt.getBoolean("Action");
		
		topItems.clear();
		ItemStackHelper.loadAllItems(nbt, topItems);
	}
	
	@Override
	public NonNullList<ItemStack> getTopItems()
	{
		return topItems;
	}
	
	@Override
	public void setTopItems(NonNullList<ItemStack> items)
	{
		topItems = items;
	}
}