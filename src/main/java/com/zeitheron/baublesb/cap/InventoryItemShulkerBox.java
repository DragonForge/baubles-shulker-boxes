package com.zeitheron.baublesb.cap;

import java.util.Iterator;

import javax.annotation.Nullable;

import com.zeitheron.baublesb.BaubleShulkerBoxes;
import com.zeitheron.baublesb.NetworkBSB;
import com.zeitheron.baublesb.api.IContainer;
import com.zeitheron.baublesb.api.ISoundOpenable;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

public class InventoryItemShulkerBox implements IInventory
{
	public final ItemStack stack;
	private NonNullList<ItemStack> items, topStacks = NonNullList.withSize(8, ItemStack.EMPTY);
	public final IContainer type;
	
	public boolean cleaned = false;
	
	public InventoryItemShulkerBox(ItemStack stack, IContainer type)
	{
		this.stack = stack;
		this.type = type;
	}
	
	public void loadFromNbt(NBTTagCompound compound)
	{
		this.items = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(compound, this.items);
	}
	
	public NBTTagCompound saveToNbt(NBTTagCompound compound)
	{
		ItemStackHelper.saveAllItems(compound, this.items, false);
		return compound;
	}
	
	@Override
	public String getName()
	{
		return stack.getDisplayName();
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentString(getName());
	}
	
	@Override
	public int getSizeInventory()
	{
		return 27;
	}
	
	@Override
	public boolean isEmpty()
	{
		for(ItemStack itemstack : items)
			if(!itemstack.isEmpty())
				return false;
		return true;
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return items.get(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return ItemStackHelper.getAndSplit(items, index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return ItemStackHelper.getAndRemove(items, index);
	}
	
	@Override
	public void setInventorySlotContents(int index, @Nullable ItemStack stack)
	{
		ItemStack o = items.get(index);
		items.set(index, stack);
		if(stack.getCount() > getInventoryStackLimit())
			stack.setCount(getInventoryStackLimit());
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public void markDirty()
	{
		// Create top stacks, not more than once per tick.
		cleaned = false;
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return true;
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
		ItemStack item = BaublesApi.getBaublesHandler(player).getStackInSlot(BaubleType.BODY.getValidSlots()[0]);
		stack.deserializeNBT(item.serializeNBT());
		
		NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
		stack.setTagCompound(nbt);
		loadFromNbt(nbt.getCompoundTag("BlockEntityTag"));
		
		if(!player.isSpectator())
		{
			player.world.playSound(null, player.getPosition(), type.getOpenSound(), SoundCategory.BLOCKS, 0.5F, player.world.rand.nextFloat() * 0.1F + 0.9F);
			
			if(player instanceof EntityPlayerMP)
			{
				IShulkerAnimation isa = player.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null);
				if(isa != null)
					isa.open();
				NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
			}
		}
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
		if(!player.isSpectator())
		{
			player.world.playSound(null, player.getPosition(), type.getCloseSound(), SoundCategory.BLOCKS, 0.5F, player.world.rand.nextFloat() * 0.1F + 0.9F);
			
			NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
			nbt.setTag("BlockEntityTag", saveToNbt(new NBTTagCompound()));
			stack.setTagCompound(nbt);
			
			BaublesApi.getBaublesHandler(player).setStackInSlot(BaubleType.BODY.getValidSlots()[0], stack.copy());
			
			if(player instanceof EntityPlayerMP)
			{
				IShulkerAnimation isa = player.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null);
				if(isa != null)
					isa.close();
				NetworkBSB.INSTANCE.syncAnimation((EntityPlayerMP) player);
			}
		}
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return true;
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		items.clear();
	}
}