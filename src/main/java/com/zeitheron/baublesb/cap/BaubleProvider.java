package com.zeitheron.baublesb.cap;

import baubles.api.IBauble;
import baubles.api.cap.BaublesCapabilities;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

public class BaubleProvider implements ICapabilityProvider
{
	IBauble bauble;
	
	public BaubleProvider(IBauble baubleIn)
	{
		this.bauble = baubleIn;
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == BaublesCapabilities.CAPABILITY_ITEM_BAUBLE;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == BaublesCapabilities.CAPABILITY_ITEM_BAUBLE ? (T) bauble : null;
	}
}