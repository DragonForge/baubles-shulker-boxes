package com.zeitheron.baublesb.cap;

import com.zeitheron.baublesb.BaubleShulkerBoxes;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;

public class ShulkerProvider implements ICapabilityProvider, INBTSerializable<NBTTagCompound>
{
	public final ShulkerImpl impl = new ShulkerImpl();
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		return impl.serializeNBT();
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		impl.deserializeNBT(nbt);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == BaubleShulkerBoxes.ANIMATION_CAPABILITY;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == BaubleShulkerBoxes.ANIMATION_CAPABILITY ? (T) impl : null;
	}
}