package com.zeitheron.baublesb.cap;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.util.INBTSerializable;

public interface IShulkerAnimation extends ITickable, INBTSerializable<NBTTagCompound>
{
	default void setPlayer(EntityPlayer player)
	{
	}
	
	default EntityPlayer getPlayer()
	{
		return null;
	}
	
	void open();
	
	void close();
	
	boolean getLastActionState();
	
	float getProgress();
	
	float getCurrentProgress(float partialTime);
	
	NonNullList<ItemStack> getTopItems();
	
	void setTopItems(NonNullList<ItemStack> items);
}