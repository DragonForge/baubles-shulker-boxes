package com.zeitheron.baublesb;

import java.io.IOException;

import com.zeitheron.baublesb.cap.IShulkerAnimation;

import io.netty.buffer.Unpooled;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.INetHandlerPlayServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLEventChannel;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import net.minecraftforge.fml.common.network.internal.FMLProxyPacket;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class NetworkBSB
{
	public static final NetworkBSB INSTANCE = null;
	
	private final FMLEventChannel channel;
	
	{
		channel = NetworkRegistry.INSTANCE.newEventDrivenChannel("baubleshulkerboxes");
		channel.register(this);
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void client(FMLNetworkEvent.ClientCustomPacketEvent e)
	{
		PacketBuffer buf = new PacketBuffer(e.getPacket().payload());
		try
		{
			NBTTagCompound nbt = buf.readCompoundTag();
			switch(nbt.getInteger("ID"))
			{
			case 0x01:
				EntityPlayer player = Minecraft.getMinecraft().world.getPlayerEntityByUUID(nbt.getUniqueId("Player"));
				IShulkerAnimation isa = player != null ? player.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null) : null;
				if(isa != null)
					isa.deserializeNBT(nbt);
			break;
			default:
			break;
			}
		} catch(IOException e1)
		{
			e1.printStackTrace();
		}
		buf.release();
	}
	
	@SubscribeEvent
	public void server(FMLNetworkEvent.ServerCustomPacketEvent e)
	{
		PacketBuffer buf = new PacketBuffer(e.getPacket().payload());
		try
		{
			NBTTagCompound nbt = buf.readCompoundTag();
			switch(nbt.getInteger("ID"))
			{
			case 0x01:
				INetHandlerPlayServer inet = e.getHandler();
				if(inet instanceof NetHandlerPlayServer)
				{
					NetHandlerPlayServer net = (NetHandlerPlayServer) inet;
					EntityPlayerMP sender = net.player;
					
					sender.openGui(BaubleShulkerBoxes.instance, 0, sender.world, sender.getPosition().getX(), sender.getPosition().getY(), sender.getPosition().getZ());
				}
			break;
			default:
			break;
			}
		} catch(IOException e1)
		{
			e1.printStackTrace();
		}
		buf.release();
	}
	
	public void syncAnimation(EntityPlayerMP mp)
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("ID", 0x01);
		tag.setUniqueId("Player", mp.getGameProfile().getId());
		IShulkerAnimation isa = mp.getCapability(BaubleShulkerBoxes.ANIMATION_CAPABILITY, null);
		if(isa != null)
			tag.merge(isa.serializeNBT());
		PacketBuffer payload = new PacketBuffer(Unpooled.buffer());
		payload.writeCompoundTag(tag);
		channel.sendToAllAround(new FMLProxyPacket(payload, "baubleshulkerboxes"), new TargetPoint(mp.world.provider.getDimension(), mp.posX, mp.posY, mp.posZ, 256));
	}
	
	@SideOnly(Side.CLIENT)
	public void onOpen()
	{
		NBTTagCompound tag = new NBTTagCompound();
		
		tag.setInteger("ID", 0x01);
		
		PacketBuffer payload = new PacketBuffer(Unpooled.buffer());
		payload.writeCompoundTag(tag);
		channel.sendToServer(new FMLProxyPacket(payload, "baubleshulkerboxes"));
	}
}