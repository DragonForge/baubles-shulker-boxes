package com.zeitheron.baublesb.proxy;

import java.util.Map;

import com.zeitheron.baublesb.NetworkBSB;
import com.zeitheron.baublesb.client.BaublesCapRenderLayer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class ClientProxy extends CommonProxy
{
	public static final KeyBinding KEY_SHULKERBOX = new KeyBinding("keybind.baubleshulkerbox", 0, "key.categories.inventory");
	
	@Override
	public void preInit()
	{
		ClientRegistry.registerKeyBinding(KEY_SHULKERBOX);
	}
	
	@Override
	public void init()
	{
		Map<String, RenderPlayer> skinMap = Minecraft.getMinecraft().getRenderManager().getSkinMap();
		
		RenderPlayer render = skinMap.get("default");
		render.addLayer(new BaublesCapRenderLayer());
		
		render = skinMap.get("slim");
		render.addLayer(new BaublesCapRenderLayer());
	}
	
	boolean open = false;
	
	@SubscribeEvent
	public void playerTick(PlayerTickEvent event)
	{
		if(event.side == Side.CLIENT && event.phase == Phase.START)
		{
			boolean copen = KEY_SHULKERBOX.isPressed() && FMLClientHandler.instance().getClient().inGameHasFocus;
			
			if(copen != open && copen)
				NetworkBSB.INSTANCE.onOpen();
			
			open = copen;
		}
	}
}