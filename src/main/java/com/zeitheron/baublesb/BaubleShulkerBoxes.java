package com.zeitheron.baublesb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.baublesb.boxes.VanillaShulkerBox;
import com.zeitheron.baublesb.cap.BaubleProvider;
import com.zeitheron.baublesb.cap.IShulkerAnimation;
import com.zeitheron.baublesb.cap.ShulkerImpl;
import com.zeitheron.baublesb.cap.ShulkerProvider;
import com.zeitheron.baublesb.intr.ironchest.IronChestAPI;
import com.zeitheron.baublesb.intr.shulko.ShulkoAPI;
import com.zeitheron.baublesb.proxy.CommonProxy;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemShulkerBox;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

@EventBusSubscriber
@Mod(modid = "baubleshulkerboxes", name = "Bauble Shulker Boxes", version = "@VERSION@", dependencies = "required-after:baubles", updateJSON = "https://pastebin.com/raw/T9481VHe", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97")
public class BaubleShulkerBoxes
{
	public static final Logger LOG = LogManager.getLogger("BaubleShulkerBoxes");
	
	@CapabilityInject(IShulkerAnimation.class)
	public static final Capability<IShulkerAnimation> ANIMATION_CAPABILITY = null;
	
	@Instance
	public static BaubleShulkerBoxes instance;
	
	@SidedProxy(serverSide = "com.zeitheron.baublesb.proxy.CommonProxy", clientSide = "com.zeitheron.baublesb.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	public static final ResourceLocation BAUBLE_CAP_PATH = new ResourceLocation("baubleshulkerboxes", "bauble");
	
	@SubscribeEvent
	public void capAttachStack(AttachCapabilitiesEvent<ItemStack> event)
	{
		ItemStack object = event.getObject();
		
		// Item is not registered? SKIP IT!
		if(object == null || object.isEmpty() || object.getItem() == null || object.getItem().getRegistryName() == null)
			return;
		
		if(Loader.isModLoaded("ironchest"))
			IronChestAPI.attachItemCap(event);
		
		if(Loader.isModLoaded("shulko"))
			ShulkoAPI.attachItemCap(event);
		
		ItemStack stack = event.getObject();
		
		// Add bauble capability ONLY if it's absent.
		if(stack.getItem() instanceof ItemShulkerBox && !event.getCapabilities().containsKey(BAUBLE_CAP_PATH))
			event.addCapability(BAUBLE_CAP_PATH, new BaubleProvider(new VanillaShulkerBox(stack)));
	}
	
	@SubscribeEvent
	public void capAttachEnt(AttachCapabilitiesEvent<Entity> event)
	{
		if(event.getObject() instanceof EntityPlayer)
			event.addCapability(new ResourceLocation("baubleshulkerboxes", "animation"), new ShulkerProvider());
	}
	
	@SubscribeEvent
	public void playerTick(PlayerTickEvent event)
	{
		if(event.phase == Phase.END)
		{
			IShulkerAnimation isa = event.player.getCapability(ANIMATION_CAPABILITY, null);
			if(isa != null)
			{
				isa.setPlayer(event.player);
				isa.update();
			}
		}
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		CapabilityManager.INSTANCE.register(IShulkerAnimation.class, new IStorage<IShulkerAnimation>()
		{
			@Override
			public NBTBase writeNBT(Capability<IShulkerAnimation> capability, IShulkerAnimation instance, EnumFacing side)
			{
				return instance == null ? new NBTTagCompound() : instance.serializeNBT();
			}
			
			@Override
			public void readNBT(Capability<IShulkerAnimation> capability, IShulkerAnimation instance, EnumFacing side, NBTBase nbt)
			{
				if(!(instance instanceof IShulkerAnimation))
					throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
				instance.deserializeNBT((NBTTagCompound) nbt);
			}
		}, ShulkerImpl::new);
		
		MinecraftForge.EVENT_BUS.register(proxy);
		MinecraftForge.EVENT_BUS.register(this);
		proxy.preInit();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiManagerBSB());
		FinalFieldHelper.setStaticFinalField(NetworkBSB.class, "INSTANCE", new NetworkBSB());
		proxy.init();
	}
	
	public static class FinalFieldHelper
	{
		private static Field modifiersField;
		private static Object reflectionFactory;
		private static Method newFieldAccessor;
		private static Method fieldAccessorSet;
		
		static boolean setStaticFinalField(Class<?> cls, String var, Object val)
		{
			try
			{
				Field f = cls.getDeclaredField(var);
				if(Modifier.isStatic(f.getModifiers()))
					return setFinalField(f, null, val);
				return false;
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
			return false;
		}
		
		static Field makeWritable(Field f) throws ReflectiveOperationException
		{
			f.setAccessible(true);
			if(modifiersField == null)
			{
				Method getReflectionFactory = Class.forName("sun.reflect.ReflectionFactory").getDeclaredMethod("getReflectionFactory");
				reflectionFactory = getReflectionFactory.invoke(null);
				newFieldAccessor = Class.forName("sun.reflect.ReflectionFactory").getDeclaredMethod("newFieldAccessor", Field.class, boolean.class);
				fieldAccessorSet = Class.forName("sun.reflect.FieldAccessor").getDeclaredMethod("set", Object.class, Object.class);
				modifiersField = Field.class.getDeclaredField("modifiers");
				modifiersField.setAccessible(true);
			}
			modifiersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
			return f;
		}
		
		public static boolean setFinalField(Field f, @Nullable Object instance, Object thing) throws ReflectiveOperationException
		{
			if(Modifier.isFinal(f.getModifiers()))
			{
				makeWritable(f);
				Object fieldAccessor = newFieldAccessor.invoke(reflectionFactory, f, false);
				fieldAccessorSet.invoke(fieldAccessor, instance, thing);
				return true;
			}
			return false;
		}
	}
}