package com.zeitheron.baublesb;

public class BSBUtil
{
	public static <T> T cast(Object val, Class<T> target)
	{
		if(val == null)
			return null;
		if(target.isAssignableFrom(val.getClass()))
			return target.cast(val);
		return null;
	}
}