package com.zeitheron.baublesb.api;

import com.zeitheron.baublesb.cap.InventoryItemShulkerBox;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.IBauble;
import baubles.api.cap.BaublesCapabilities;
import net.minecraft.client.gui.inventory.GuiShulkerBox;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerShulkerBox;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public interface IContainer extends ISoundOpenable
{
	ResourceLocation getBoxTextures(ItemStack stack);
	
	int getSlotCount(ItemStack stack);
	
	public InventoryItemShulkerBox getInventory(EntityPlayer player, ItemStack stack);
	
	public Object createGui(EntityPlayer player, IInventory inv, ItemStack stack);
	
	public Object createContainer(EntityPlayer player, IInventory inv, ItemStack stack);
	
	static IContainer getContainer(ItemStack stack)
	{
		if(stack.isEmpty())
			return null;
		if(stack.getItem() instanceof IContainer)
			return (IContainer) stack.getItem();
		if(stack.hasCapability(BaublesCapabilities.CAPABILITY_ITEM_BAUBLE, null))
		{
			IBauble bauble = stack.getCapability(BaublesCapabilities.CAPABILITY_ITEM_BAUBLE, null);
			if(bauble instanceof IContainer)
				return (IContainer) bauble;
		}
		return null;
	}
}