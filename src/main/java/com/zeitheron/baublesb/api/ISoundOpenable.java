package com.zeitheron.baublesb.api;

import net.minecraft.util.SoundEvent;

public interface ISoundOpenable
{
	SoundEvent getCloseSound();
	
	SoundEvent getOpenSound();
}