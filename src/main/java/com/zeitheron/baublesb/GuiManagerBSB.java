package com.zeitheron.baublesb;

import com.zeitheron.baublesb.api.IContainer;
import com.zeitheron.baublesb.cap.InventoryItemShulkerBox;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiManagerBSB implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
		case 0:
			ItemStack item = BaublesApi.getBaublesHandler(player).getStackInSlot(BaubleType.BODY.getValidSlots()[0]);
			IContainer container = IContainer.getContainer(item);
			if(container != null)
			{
				InventoryItemShulkerBox inv = container.getInventory(player, item);
				if(inv != null)
					return container.createContainer(player, inv, item);
			}
		break;
		default:
			return null;
		}
		return null;
	}
	
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
		case 0:
			ItemStack item = BaublesApi.getBaublesHandler(player).getStackInSlot(BaubleType.BODY.getValidSlots()[0]);
			IContainer container = IContainer.getContainer(item);
			if(container != null)
			{
				InventoryItemShulkerBox inv = container.getInventory(player, item);
				if(inv != null)
					return container.createGui(player, inv, item);
			}
		break;
		default:
			return null;
		}
		return null;
	}
}