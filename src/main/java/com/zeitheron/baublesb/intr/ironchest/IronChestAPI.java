package com.zeitheron.baublesb.intr.ironchest;

import com.zeitheron.baublesb.BaubleShulkerBoxes;
import com.zeitheron.baublesb.boxes.IronShulkerBox;
import com.zeitheron.baublesb.cap.BaubleProvider;

import cpw.mods.ironchest.common.items.shulker.ItemIronShulkerBox;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.AttachCapabilitiesEvent;

public class IronChestAPI
{
	public static void attachItemCap(AttachCapabilitiesEvent<ItemStack> event)
	{
		ItemStack stack = event.getObject();
		
		if(stack.getItem() instanceof ItemIronShulkerBox)
			event.addCapability(BaubleShulkerBoxes.BAUBLE_CAP_PATH, new BaubleProvider(new IronShulkerBox(stack)));
	}
}