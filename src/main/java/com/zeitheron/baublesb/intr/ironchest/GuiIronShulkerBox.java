package com.zeitheron.baublesb.intr.ironchest;

import cpw.mods.ironchest.common.blocks.shulker.IronShulkerBoxType;
import cpw.mods.ironchest.common.gui.shulker.ContainerIronShulkerBox;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.util.ResourceLocation;

public class GuiIronShulkerBox extends GuiContainer
{
	ResourceLocation tex;
	
	public GuiIronShulkerBox(InventoryPlayer playerInv, IInventory shulkerInventory, IronShulkerBoxType type, int xSize, int ySize)
	{
		super(new ContainerIronShulkerBox(playerInv, shulkerInventory, type, xSize, ySize));
		String tty = type.name().toLowerCase();
		
		if(tty.equals("obsidian") || tty.equals("crystal"))
			tty = "diamond";
		
		tex = new ResourceLocation("ironchest", "textures/gui/" + tty + "_container.png");
		this.xSize = xSize;
		this.ySize = ySize;
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		mc.getTextureManager().bindTexture(tex);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
}