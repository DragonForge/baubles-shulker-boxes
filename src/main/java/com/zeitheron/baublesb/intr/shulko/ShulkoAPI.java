package com.zeitheron.baublesb.intr.shulko;

import com.zeitheron.baublesb.BaubleShulkerBoxes;
import com.zeitheron.baublesb.cap.BaubleImpl;
import com.zeitheron.baublesb.cap.BaubleProvider;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;

public class ShulkoAPI
{
	public static void attachItemCap(AttachCapabilitiesEvent<ItemStack> event)
	{
		ResourceLocation item = event.getObject().getItem().getRegistryName();
		if(item.getNamespace().equalsIgnoreCase("shulko") && item.getPath().equalsIgnoreCase("actually_purple_shulker_box"))
			event.addCapability(BaubleShulkerBoxes.BAUBLE_CAP_PATH, new BaubleProvider(new BaubleImpl(event.getObject(), new ResourceLocation("shulko", "textures/entity/shulker_actually_purple.png"), 27)));
	}
}